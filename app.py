# mybot/app.py

import os
from flask import (
    Flask, request, abort
)
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import InvalidSignatureError
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)

app = Flask(__name__)

LINE_CHANNEL_ACCESS_TOKEN = os.environ['LINE_CHANNEL_ACCESS_TOKEN']
LINE_CHANNEL_SECRET = os.environ['LINE_CHANNEL_SECRET']

line_bot_api = LineBotApi(LINE_CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(LINE_CHANNEL_SECRET)

sender_to_receiver = {}
pendengar_list = []
pencurhat_list = []


class InfoMessage:
    HELP_TEXT = "Berikut command yang dapat digunakan :\n\n" + \
                "/pengen_curhat : meminta orang lain yang bersedia untuk dengerin curhatan kamu\n\n" + \
                "/dengar_curhat : menjadi relawan untuk orang lain yang sedang ingin curhat\n\n" + \
                "/batalkan : keluar dari waiting list curhat atau pendengar curhat\n\n" + \
                "/berhenti : memberhentikan sesi yang sedang aktif, pastikan teman bicaramu tau ya!\n\n" + \
                "/bantuan : menampilkan pesan bantuan\n\n" + \
                "/status: menampilkan status kamu dan waiting list serta sesi aktif yang ada"

    PENGEN_CURHAT_AVAILABLE = "Hai! ada yang mau dengerin curhatan kamu nih, silakan ya!"
    PENGEN_CURHAT_WAIT = "Lagi gaada yang bisa jadi teman curhat nih! tapi aku masukin kamu ke waiting list ya!!"
    PENGEN_CURHAT_EXIT = "Kamu telah keluar dari waiting list curhat!"

    DENGAR_CURHAT_AVAILABLE = "Hai! ada yang mau curhat sama kamu nih, silakan ya!"
    DENGAR_CURHAT_WAIT = "Lagi gaada yang butuh teman curhat nih! tapi aku masukin kamu ke waiting list ya!!"
    DENGAR_CURHAT_EXIT = "Kamu telah keluar dari waiting list pendengar curhat!"

    IN_PENGEN_CURHAT_LIST = "Kamu sedang berada pada waiting list orang yang pengen curhat"
    IN_DENGAR_CURHAT_LIST = "Kamu sedang berada pada waiting list pendengar curhat"
    NOT_IN_WAITING_LIST = "Kamu sedang tidak ada di waiting list manapun!"

    SESSION_EXIST = "Kamu lagi ada sesi aktif sama orang lain!"
    SESSION_NOT_EXIST = "Kamu tidak memiliki sesi yang sedang aktif"
    SESSION_EXIT = "Sesi aktif telah diberhentikan!"
    SESSION_EXITED = "Pihak sebelah telah memberhentikan sesi aktif ini!"

    @staticmethod
    def status_list():
        return "Jumlah waiting list pencurhat: {}\n".format(len(pencurhat_list)) + \
               "Jumlah waiting list pendengar: {}\n".format(len(pendengar_list)) + \
               "Jumlah sesi aktif: {}".format(len(sender_to_receiver))

    @staticmethod
    def status(sender_id):
        if sender_id in pencurhat_list:
            return "{}\n\n{}".format(
                InfoMessage.IN_PENGEN_CURHAT_LIST+" dengan urutan "+str(pencurhat_list.index(sender_id)+1),
                InfoMessage.status_list())
        elif sender_id in pendengar_list:
            return "{}\n\n{}".format(
                InfoMessage.IN_DENGAR_CURHAT_LIST+" dengan urutan "+str(pendengar_list.index(sender_id)+1),
                InfoMessage.status_list())
        elif sender_id in sender_to_receiver.keys():
            return "{}\n\n{}".format(InfoMessage.SESSION_EXIST, InfoMessage.status_list())
        else:
            return "{}\n\n{}".format(InfoMessage.NOT_IN_WAITING_LIST, InfoMessage.status_list())
    

def send_to_receiver(receiver_id, text_message):
    line_bot_api.push_message(receiver_id, TextSendMessage(text=text_message))


def add_sender_receiver(sender_id, receiver_id):
    sender_to_receiver[sender_id] = receiver_id
    sender_to_receiver[receiver_id] = sender_id


def clear_sender_receiver(sender_id, receiver_id):
    sender_to_receiver.pop(sender_id, None)
    sender_to_receiver.pop(receiver_id, None)


@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):
    text_message = event.message.text
    sender_id = event.source.user_id

    if text_message == "/ReseT":
        pencurhat_list.clear()
        pendengar_list.clear()
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text="Sipp udah direset ya bous"))

    elif text_message.lower() == "/bantuan":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.HELP_TEXT))

    elif text_message.lower() == "/pengen_curhat":
        if sender_id in pendengar_list:
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.IN_DENGAR_CURHAT_LIST))
        elif len(pendengar_list) > 0:
            if sender_id not in sender_to_receiver:
                receiver_id = pendengar_list[0]
                pendengar_list.remove(receiver_id)
                add_sender_receiver(sender_id, receiver_id)

                send_to_receiver(sender_id, InfoMessage.PENGEN_CURHAT_AVAILABLE)
                send_to_receiver(receiver_id, InfoMessage.DENGAR_CURHAT_AVAILABLE)
            else:
                line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.SESSION_EXIST))
        else:
            pencurhat_list.append(sender_id)
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.PENGEN_CURHAT_WAIT))

    elif text_message.lower() == "/dengar_curhat":
        if sender_id in pencurhat_list:
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.IN_PENGEN_CURHAT_LIST))
        elif len(pencurhat_list) > 0:
            if sender_id not in sender_to_receiver:
                receiver_id = pencurhat_list[0]
                pencurhat_list.remove(receiver_id)
                add_sender_receiver(sender_id, receiver_id)

                send_to_receiver(sender_id, InfoMessage.DENGAR_CURHAT_AVAILABLE)
                send_to_receiver(receiver_id, InfoMessage.PENGEN_CURHAT_AVAILABLE)
            else:
                line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.SESSION_EXIST))
        else:
            pendengar_list.append(sender_id)
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.DENGAR_CURHAT_WAIT))

    elif text_message.lower() == "/batalkan":
        if sender_id in pencurhat_list:
            pencurhat_list.remove(sender_id)
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.PENGEN_CURHAT_EXIT))
        elif sender_id in pendengar_list:
            pendengar_list.remove(sender_id)
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.DENGAR_CURHAT_EXIT))
        else:
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.NOT_IN_WAITING_LIST))

    elif text_message.lower() == "/berhenti":
        if sender_id in sender_to_receiver:
            receiver_id = sender_to_receiver[sender_id]

            clear_sender_receiver(sender_id, receiver_id)

            line_bot_api.push_message(sender_id, TextSendMessage(text=InfoMessage.SESSION_EXIT))
            line_bot_api.push_message(receiver_id, TextSendMessage(text=InfoMessage.SESSION_EXITED))
        else:
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.SESSION_NOT_EXIST))

    elif text_message.lower() == "/status":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.status(sender_id)))

    elif text_message[0] == '/':
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.HELP_TEXT))

    elif sender_id in sender_to_receiver:
        try:
            receiver_id = sender_to_receiver[sender_id]
            send_to_receiver(receiver_id, text_message)
        except KeyError:
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.SESSION_NOT_EXIST))

    elif sender_id in pencurhat_list:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.PENGEN_CURHAT_WAIT))

    elif sender_id in pendengar_list:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text=InfoMessage.DENGAR_CURHAT_WAIT))

    else:
        send_to_receiver(sender_id, InfoMessage.SESSION_NOT_EXIST)
        send_to_receiver(sender_id, InfoMessage.HELP_TEXT)


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
